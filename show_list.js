'use strict'

function showList(arr, parent = document.body) {

    let list = document.createElement('ul');
    arr.forEach(e => {

        let listItem = document.createElement('li');

        if (Array.isArray(e)) {
            showList(e, listItem);
        } else {
         listItem.textContent = e;
        }

        list.append(listItem);
    });
    parent.append(list);
}

showList(["hello", ['Chernihiv', 'Poltava'], "Kyiv", "Kharkiv", "Odessa", "Lviv"]);

let counter = 3;
const intervalId = setInterval(() => {
    alert(counter);
  counter -= 1;
if (counter === 0) {
    clearInterval(intervalId);
    document.body.remove();
  }
}, 1000);